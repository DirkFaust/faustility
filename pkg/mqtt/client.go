package mqtt

import (
	"fmt"
	"os"
	"strconv"
	"time"

	mqttLibrary "github.com/goiiot/libmqtt"
	log "github.com/sirupsen/logrus"
)

const (
	defaultMqttHost = "mqtt"
	defaultMqttPort = 1883

	envMqttHost     = "MQTT_HOST"
	envMqttPort     = "MQTT_PORT"
	envMqttClientID = "MQTT_CLIENT_ID"
	envMqttUsername = "MQTT_USERNAME"
	envMqttPassword = "MQTT_PASSWORD"
)

type Message struct {
	Topic   string
	Payload []byte
}

type SendFunc func(message Message, retain bool)

func connectionString() string {
	host := os.Getenv(envMqttHost)
	if len(host) == 0 {
		host = defaultMqttHost
	}
	port, err := strconv.Atoi(os.Getenv(envMqttPort))
	if err != nil {
		port = defaultMqttPort
	}

	return fmt.Sprintf("%v:%v", host, port)
}

// Client represents a MQTT client with auto re-subscription capabilities,
type Client struct {
	inner  *mqttLibrary.AsyncClient
	topics []*mqttLibrary.Topic
	kill   chan bool
}

// NewClient creates a new Client. Topics will be (re)subscribed on (re)connect.
// TODO: make subscriptions optional
func NewClient(willPacket *Message, topics ...string) Client {
	options := []mqttLibrary.Option{
		mqttLibrary.WithKeepalive(60, 1.2),
		mqttLibrary.WithAutoReconnect(true),
		mqttLibrary.WithBackoffStrategy(time.Second, 5*time.Second, 1.2),
		mqttLibrary.WithRouter(mqttLibrary.NewRegexRouter()),
	}

	if willPacket != nil {
		options = append(options, mqttLibrary.WithWill(willPacket.Topic, mqttLibrary.Qos1, true, willPacket.Payload))
	}

	clientID := os.Getenv(envMqttClientID)
	if len(clientID) != 0 {
		log.Debugf("Using MQTT clientID %s", clientID)
		options = append(options, mqttLibrary.WithClientID(clientID))
	}

	username := os.Getenv(envMqttUsername)
	password := os.Getenv(envMqttPassword)

	if len(username) != 0 && len(password) != 0 {
		log.Debugf("Using MQTT credentials %s:*******", username)
		options = append(options, mqttLibrary.WithIdentity(username, password))

	}

	client, err := mqttLibrary.NewClient(
		options...,
	)

	if err != nil {
		panic(fmt.Errorf("unable to create MQTT client: %v", err))
	}

	var mqttTopics []*mqttLibrary.Topic
	for _, topic := range topics {
		mqttTopics = append(mqttTopics, &mqttLibrary.Topic{
			Name: topic,
			Qos:  mqttLibrary.Qos1,
		})
	}

	return Client{
		client,
		mqttTopics,
		make(chan bool),
	}
}

func (client *Client) Publish(message Message, retain bool) {
	log.Debugf("MQTT publishing %v -> %v", message.Topic, string(message.Payload))
	client.inner.Publish(&mqttLibrary.PublishPacket{
		TopicName: message.Topic,
		Payload:   message.Payload,
		Qos:       mqttLibrary.Qos1,
		IsRetain:  retain,
	})
}

func (client *Client) Close() {
	log.Debugf("Closing MQTT client")
	client.inner.Destroy(true)
	client.kill <- true
}

// Generate creates a generator for incoming messages from the mqtt bus
func (client *Client) Generate() chan Message {
	messageBus := make(chan Message)

	err := client.inner.ConnectServer(connectionString(),
		mqttLibrary.WithCustomTLS(nil),
		mqttLibrary.WithConnHandleFunc(func(asyncClient mqttLibrary.Client, server string, code byte, err error) {
			logLine := fmt.Sprintf("Connection to MQTT. Server %v, Code %v, err %v", server, code, err)
			if err != nil || code != mqttLibrary.CodeSuccess {
				log.Fatal(logLine)
				close(messageBus)
			} else {
				log.Debug(logLine)
				asyncClient.Subscribe(client.topics...)
			}
		}))

	if err != nil {
		log.Errorf("Error while connecting to MQTT server %v: %v", connectionString(), err)
		close(messageBus)
	}

	go func() {
		<-client.kill
		log.Info("Received kill")
		close(messageBus)
	}()

	client.inner.HandleTopic(".*", func(client mqttLibrary.Client, topic string, qos mqttLibrary.QosLevel, msg []byte) {
		messageBus <- Message{
			Topic:   topic,
			Payload: msg,
		}
	})

	return messageBus
}
