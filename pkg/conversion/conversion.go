package conversion

import (
	"fmt"
	"reflect"
	"strconv"
	"time"
)

type SupportedType interface {
	int | int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64 | float32 | float64 | time.Duration
}

// FromString converts an input string to a given `out`-type
func FromString[V SupportedType](in string, out *V) error {
	var err error
	var r any
	t := reflect.TypeOf(*out)
	x := reflect.ValueOf(*out).Interface()
	switch x.(type) {
	case time.Duration:
		r, err = time.ParseDuration(in)
		*out = V(r.(time.Duration))
	case int,
		int8,
		int16,
		int32,
		int64:
		r, err = strconv.ParseInt(in, 10, t.Bits())
		*out = V(r.(int64))
	case uint,
		uint8,
		uint16,
		uint32,
		uint64:
		r, err = strconv.ParseUint(in, 10, t.Bits())
		*out = V(r.(uint64))
	case float32,
		float64:
		r, err = strconv.ParseFloat(in, t.Bits())
		*out = V(r.(float64))
	default:
		err = fmt.Errorf("unsupported type")
	}

	return err
}

// FromStringOrDie converts the given string to a `Number` or panics`
func FromStringOrDie[V SupportedType](in string, out *V) {
	if err := FromString(in, out); err != nil {
		panic(err)
	}
}
