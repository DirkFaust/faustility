package audiocrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
)

type AudioEncryptor struct {
	KeyContainer
}

func NewAudioEncryptor(password string, salt Salt) AudioEncryptor {
	return AudioEncryptor{
		KeyContainer: KeyContainer{DeriveKey(password, salt), salt},
	}
}

func (e AudioEncryptor) Encrypt(pcm []int16) ([]byte, error) {
	block, err := aes.NewCipher(e.key)
	if err != nil {
		return nil, err
	}

	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, aesGCM.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	bytes := FromPCM(pcm)
	encrypted := aesGCM.Seal(nonce, nonce, bytes, nil)
	complete := append(e.KeyContainer.salt.Buffer, encrypted...)

	return complete, nil
}
