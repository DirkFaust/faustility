package audiocrypt

import (
	"testing"
)

func TestFromToPCM(t *testing.T) {
	input := []int16{0, 1, 2, 3, 4, 5, -5, -4, -3, -2, -1}
	bytes := FromPCM(input)
	reconstructed := ToPCM(bytes)

	for i, val := range input {
		if val != reconstructed[i] {
			t.Fatalf("Input and Output should be identical. Got %d, want %d at index %d", reconstructed[i], val, i)
		}
	}
}
