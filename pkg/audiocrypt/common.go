package audiocrypt

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"io"

	"golang.org/x/crypto/pbkdf2"
)

const SaltLen = 8

type Salt struct {
	Buffer []byte
}

func NewSalt(b []byte) Salt {
	return Salt{
		Buffer: b,
	}
}

func RandomSalt() Salt {
	saltBytes := make([]byte, SaltLen)
	_, err := io.ReadFull(rand.Reader, saltBytes)
	if err != nil {
		panic(fmt.Errorf("random read failed: %v", err))
	}
	return Salt{saltBytes}
}

func (s *Salt) Equals(other *Salt) bool {
	if len(s.Buffer) != len(other.Buffer) {
		return false
	}

	for i, val := range s.Buffer {
		if val != other.Buffer[i] {
			return false
		}
	}

	return true
}

type KeyContainer struct {
	key  []byte
	salt Salt
}

func DeriveKey(password string, salt Salt) []byte {
	return pbkdf2.Key([]byte(password), salt.Buffer, 4096, 32, sha256.New)
}

func FromPCM(pcm []int16) []byte {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.LittleEndian, pcm)
	return buf.Bytes()
}

func ToPCM(data []byte) []int16 {
	convBuffer := make([]int16, len(data)>>1)

	buf := new(bytes.Buffer)
	buf.Write(data)
	binary.Read(buf, binary.LittleEndian, convBuffer)

	return convBuffer
}
