package audiocrypt

import "testing"

func TestCrypt(t *testing.T) {
	input := []int16{0, 1, 2, 3, 4, 5, -5, -4, -3, -2, -1, -32767, 32767}

	enc := NewAudioEncryptor("foo", RandomSalt())
	dec := NewAudioDecryptor("foo")

	encoded, err := enc.Encrypt(input)
	if err != nil {
		t.Fatal(err)
	}

	decoded, err := dec.Decrypt(encoded)
	if err != nil {
		t.Fatal(err)
	}

	for i, val := range input {
		if decoded[i] != val {
			t.Fatalf("Decrypted index %d does not match - expected %d got %d", i, val, decoded[i])
		}
	}
}
