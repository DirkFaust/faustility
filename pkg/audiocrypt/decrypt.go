package audiocrypt

import (
	"crypto/aes"
	"crypto/cipher"

	log "github.com/sirupsen/logrus"
)

type AudioDecryptor struct {
	KeyContainer
	// Its not just such a good idea to hold the password unencrypted in memory all the time...
	password string
}

func NewAudioDecryptor(password string) AudioDecryptor {
	return AudioDecryptor{
		KeyContainer: KeyContainer{},
		password:     password,
	}
}

func (d *AudioDecryptor) Decrypt(data []byte) ([]int16, error) {
	salt := NewSalt(data[:SaltLen])
	if !salt.Equals(&d.KeyContainer.salt) {
		log.Infof("Using new salt of len %d", len(salt.Buffer))
		d.KeyContainer = KeyContainer{DeriveKey(d.password, salt), salt}
	}

	block, err := aes.NewCipher(d.key)
	if err != nil {
		return nil, err
	}
	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	nonceSize := aesGCM.NonceSize()

	nonce, encryptedData := data[SaltLen:SaltLen+nonceSize], data[nonceSize+SaltLen:]
	decoded, err := aesGCM.Open(nil, nonce, encryptedData, nil)
	if err != nil {
		return nil, err
	}

	result := ToPCM(decoded)
	return result, nil
}
