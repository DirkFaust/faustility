# functional helpers using `any`

## Use when
You can or want to live with `any` as the contained value.
If you need additional type safety then use the package `genericfunctional` instead.

## WIP!
Contains `Option` and `Result` which I personally miss a lot from rust.
`map`-functionality is object-bound and should equal (without type checking) the rust version.
