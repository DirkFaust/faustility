package functional

import (
	"fmt"
	"reflect"
)

// See https://doc.rust-lang.org/std/Option/enum.Option.html
type Option struct {
	inner *any
}

func (o Option) String() string {
	if o.inner == nil {
		return "None"
	}

	return fmt.Sprintf("Some(%v %v)", reflect.TypeOf(*o.inner), *o.inner)
}

// Equals compares two Option.
// Returns true if the Options are equal.
// Note: standard comparison would compare if the Options are _the same_ which
// normally would lead to a `false` result as the standard comapre would compare the
// inner pointers and not their values.
func (o Option) Equals(other Option) bool {
	if o.inner == nil && other.inner == nil {
		return true
	}

	if (o.IsSome() && other.IsNone()) || (o.IsNone() && other.IsSome()) {
		return false
	}

	a, b := *o.inner, *other.inner

	return a == b
}

// None returns a None option
func None() Option {
	return Option{}
}

// Some returns a Some option (containing v)
func Some(v any) Option {
	return Option{inner: &v}
}

// IsSome returns true if the option is Some
func (o Option) IsSome() bool {
	return o.inner != nil
}

// IsNone returns true if the option is None
func (o Option) IsNone() bool {
	return !o.IsSome()
}

// Expect returns the contained Some value or panics with given message if None
func (o Option) Expect(msg string) any {
	if o.inner == nil {
		panic(msg)
	}

	return *o.inner
}

// Unwrap returns the contained Some value or panics if None
func (o Option) Unwrap() any {
	if o.inner == nil {
		panic("tried to unwrap on a None-Option")
	}

	return *o.inner
}

// UnwrapOption returns the contained Some value converted to T.
// Panics if the Option is None or T is not of the same type as the contained value.
func UnwrapOption[T any](o Option) T {
	return o.Unwrap().(T)
}

// UnwrapOr returns the contained Some value or a provided default.
func (o Option) UnwrapOr(v any) any {
	if o.inner == nil {
		return v
	}

	return *o.inner
}

// UnwrapOrElse returns the contained Some value or computes it from a lambda.
func (o Option) UnwrapOrElse(f func() any) any {
	if o.inner != nil {
		return *o.inner
	}

	return f()
}

// Map maps an Option of type T to Option of type U by applying a function to a contained value
// if the Option[T] is Some.
func (o Option) Map(f func(in any) any) Option {
	if o.IsNone() {
		return None()
	}

	return Some(f(o.Unwrap()))
}

// And returns None if the option is None, otherwise returns v.
func (o Option) And(v Option) Option {
	if o.inner == nil {
		return o
	}
	return v
}

// AndThen returns None if the option is None, otherwise calls f with the wrapped value and returns the result.
func (o Option) AndThen(f func(v any) Option) Option {
	if o.inner == nil {
		return Option{}
	}

	return f(*o.inner)
}

// Or returns the option if it contains a value, otherwise returns v.
func (o Option) Or(v Option) Option {
	if o.inner != nil {
		return o
	}
	return v
}

// OrElse returns the option if it contains a value, otherwise calls f and returns the result.
func (o Option) OrElse(f func() Option) Option {
	if o.inner != nil {
		return o
	}

	return f()
}

// Take takes the value out of the option, leaving a None in its place.
func (o *Option) Take() any {
	if o.inner == nil {
		panic("tried to take a None-Option")
	}

	temp := *o.inner
	o.inner = nil
	return temp
}

// Replacce replaces the actual value in the option by the value given in parameter, returning the old value if present, leaving a Some in its place.
func (o *Option) Replace(v any) Option {
	temp := o.inner
	o.inner = &v
	return Option{inner: temp}
}

// Insert inserts value into the option, then returns a mutable reference to it.
// If the option already contains a value, the old value is dropped.
func (o *Option) Insert(v any) *any {
	o.inner = &v
	return o.inner
}

// GetOrInsert inserts value into the option if it is None, then returns a mutable reference to the contained value.
func (o *Option) GetOrInsert(v any) *any {
	if o.inner == nil {
		o.inner = &v
	}
	return o.inner
}

// OrOr transforms the Option[T] into a Result[T], mapping Some(v) to Ok(v) and None to Err(err).
func (o Option) OkOr(err error) Result {
	if o.inner == nil {
		return Err(err)
	}

	return Ok(*o.inner)
}

// OkOrElse transforms the Option[T] into a Result[T], mapping Some(v) to Ok(v) and None to Err(err()) using the
// given lambda f.
func (o Option) OkOrElse(f func() Result) Result {
	return f()
}
