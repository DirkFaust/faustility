package functional

import (
	"fmt"
	"reflect"
)

// see https://doc.rust-lang.org/std/result/enum.Result.html
type Result struct {
	inner *any
	err   *error
}

// ResultFrom builds a Result from the given value and error.
// If err is not nil, the Result will be Err(err), otherwise it will be Ok(v).
func ResultFrom(v any, err error) Result {
	if err == nil {
		return Ok(v)
	} else {
		return Err(err)
	}
}

// UnwrapResult extracts the wrapped value as given Type T.
// Panics if the Result is an Err or the type does not match the wrapped type.
func UnwrapResult[T any](r Result) T {
	return r.Unwrap().(T)
}

// String is a stringer for Result
func (r Result) String() string {
	if r.inner == nil && r.err != nil {
		return fmt.Sprintf("Err(%v)", *r.err)
	}

	if r.inner != nil {
		return fmt.Sprintf("Ok(%v %v)", reflect.TypeOf(*r.inner), *r.inner)
	}

	panic("Neither ERR or OK Result")
}

// Equals compares two Results whereas == would compare if Results are the _same_, Equals
// will compare the values.
func (r Result) Equals(other Result) bool {
	if (r.IsErr() && other.IsOk()) || (r.IsOk() && other.IsErr()) {
		return false
	}

	if r.IsErr() && other.IsErr() {
		return *r.err == *other.err
	}

	if r.inner == nil && other.inner == nil {
		return true
	}

	a, b := *r.inner, *other.inner

	return a == b
}

// Ok constructs a Result wrapping v
func Ok(v any) Result {
	return Result{
		inner: &v,
		err:   nil,
	}
}

// Err constructs a Result that as an Error
func Err(err error) Result {
	return Result{
		inner: nil,
		err:   &err,
	}
}

// IsErr returns true if the result is an error
func (r Result) IsErr() bool {
	return r.err != nil
}

// IsOk returns true if the result contains a value
func (r Result) IsOk() bool {
	return r.inner != nil
}

// Unwrap returns the wrapped value or panics if the result is an error.
func (r Result) Unwrap() any {
	if r.inner == nil {
		panic("unwrap on an Err Result")
	}

	return *r.inner
}

// UnwrapOr returns the contained Ok value or a provided default.
func (r Result) UnwrapOr(v any) any {
	if r.inner == nil {
		return v
	}

	return *r.inner
}

// UnwrapOrElse returns the contained Ok value or computes it from a lambda.
func (r Result) UnwrapOrElse(f func() any) any {
	if r.inner == nil {
		return f()
	}

	return *r.inner
}

// Ok converts from Result<T, E> to Option<T>.
// If the result contains a value, the returned value contains the results value, None otherwise.
func (r Result) Ok() Option {
	if r.inner == nil {
		return None()
	}

	return Some(*r.inner)
}

// And returns v if the result is Ok, otherwise returns the Err value of self.
func (r Result) And(v Result) Result {
	if r.inner != nil {
		return v
	}
	return r
}

// AndThen calls f if the result is Ok, otherwise returns the Err value of self.
func (r Result) AndThen(f func(v any) Result) Result {
	if r.inner == nil {
		return r
	}
	return f(*r.inner)
}

// IsOkAnd returns true if the result is Ok and the value inside of it matches a predicate.
func (r Result) IsOkAnd(f func(v *any) bool) bool {
	if r.inner == nil {
		return false
	}
	return f(r.inner)
}

// Or returns v if the result is Err, otherwise returns the Ok value of self.
func (r Result) Or(v Result) Result {
	if r.inner == nil {
		return v
	}
	return r
}

// OrElse calls f if the result is Err, otherwise returns the Ok value of self.
func (r Result) OrElse(f func(e error) Result) Result {
	if r.inner == nil {
		return f(*r.err)
	}
	return r
}

// Expect returns the contained Ok value, or panics if the result is Err with given msg.
func (r Result) Expect(msg string) any {
	if r.inner == nil {
		panic(msg)
	}
	return *r.inner
}

// Map aps a Result[T] to Result[¨] by applying a function to a contained Ok value, leaving an Err value untouched.
// This function can be used to compose the results of two functions.
func (r Result) Map(f func(in any) any) Result {
	if r.inner == nil {
		return Err(*r.err)
	}
	return Ok(f(*r.inner))
}

// MapErr maps a Result[T, E] to Result[T, F] by applying a function to a contained Err value, leaving an Ok value untouched.
func (r Result) MapErr(f func(e error) error) Result {
	if r.inner == nil {
		return Err(f(*r.err))
	}
	return r
}

// MapOr returns the provided default v (if Err), or applies a function to the contained value (if Ok),
func (r Result) MapOr(v any, f func(any) any) any {
	if r.inner == nil {
		return v
	}

	return f(*r.inner)
}

// MapOrElse maps a Result<T, E> to U by applying fallback function default to a contained Err value, or function f to a contained Ok value.
// This function can be used to unpack a successful result while handling an error.
func (r Result) MapOElse(d func(error) any, f func(any) any) any {
	if r.inner == nil {
		return d(*r.err)
	}

	return f(*r.inner)
}
