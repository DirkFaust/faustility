package functional

import (
	"errors"
	"strconv"
	"testing"
)

func siconv(v any) any {
	r, err := strconv.ParseInt(v.(string), 10, 64)
	if err != nil {
		return -1
	}
	return int(r)
}

func testFunc(v any) Result {
	if v != nil {
		return Ok(v)
	} else {
		return Err(ErrSome)
	}
}

func double(v any) Result {
	return Ok(v.(int) * 2)
}

var ErrSome = errors.New("some error")
var ErrFoo = errors.New("Foo")

func TestAnd(t *testing.T) {
	res := testFunc(123).
		AndThen(double).
		Or(Ok(4711))

	if !res.Equals(Ok(246)) {
		t.Error("Expected 246")
	}

	res = testFunc(nil).
		AndThen(double).
		Or(Ok(4711))

	if !res.Equals(Ok(4711)) {
		t.Error("Expected 4711")
	}
}

func TestErr(t *testing.T) {
	res := testFunc(nil)
	if !res.Equals(Err(ErrSome)) {
		t.Errorf("Expected some-error, got %v", res)
	}

	res = res.MapErr(func(err error) error { return ErrFoo })
	if !res.Equals(Err(ErrFoo)) {
		t.Errorf("Expected foo-error, got %v", res)
	}

}

func TestMap(t *testing.T) {
	i := Ok("42").Map(siconv)
	if !i.Equals(Ok(42)) {
		t.Errorf("Exepcted 42 got %v", i)
	}

	o := Ok(42).Map(func(v any) any { return v.(int) * 2 })
	if !o.Equals(Ok(84)) {
		t.Errorf("Exepcted 48 got %v", o)
	}

	a := Err(errors.New("Foo")).Map(siconv)
	if a.IsOk() {
		t.Errorf("Err values should not call Mapping")
	}
}

func TestOk(t *testing.T) {
	a := Ok("foobar").Ok()
	if a.IsNone() {
		t.Errorf("Should be some")
	}
}

func TestAndThen(t *testing.T) {
	a := Ok(42).AndThen(func(v any) Result {
		return Ok(v.(int) * 2)
	})
	if a.IsErr() {
		t.Errorf("Should be OK")
	}

	if a.Unwrap() != 84 {
		t.Errorf("Expected 84 got %v", a)
	}
}

func TestResult(t *testing.T) {
	ok := Ok(42)
	ok2 := Ok(42)
	ok3 := Ok("foo")
	ok4 := Ok("bar")

	if !ok.Equals(ok2) {
		t.Errorf("Expected equality between %v and %v", ok, ok2)
	}

	if ok3.Equals(ok4) {
		t.Errorf("Did not expected equality between %v and %v", ok3, ok4)
	}

	if ok.IsErr() {
		t.Error("Did not expect Error")
	}

	if !ok.IsOk() {
		t.Error("Expected Ok")
	}

	foo := ok.Unwrap()
	if foo != 42 {
		t.Errorf("Expected 42, got %v", foo)
	}

	e := Err(errors.New("some error"))
	if !e.IsErr() {
		t.Error("Expected Err")
	}

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("expected panic on err unwrap")
		}
	}()

	_ = e.Unwrap()
}
