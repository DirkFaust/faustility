package env

import (
	"fmt"
	"os"
)

func GetOrDie(env string) string {
	result := os.Getenv(env)
	if len(result) == 0 {
		panic(fmt.Sprintf("Required ENV '%s' not given", env))
	}

	return result
}

func GetOrDefault(env string, orDefault string) string {
	result := os.Getenv(env)
	if len(result) == 0 {
		return orDefault
	}

	return result
}
