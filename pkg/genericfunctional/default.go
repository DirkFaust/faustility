package genericfunctional

type Default[T any] interface {
	Default() T
}
