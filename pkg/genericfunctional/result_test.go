package genericfunctional

import (
	"errors"
	"strconv"
	"testing"
)

func siconv(v string) int {
	r, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return -1
	}
	return int(r)
}

func TestMap(t *testing.T) {
	i := Ok("42").MapI(siconv)
	if !i.Equals(Ok(42)) {
		t.Errorf("Exepcted 42 got %v", i)
	}

	a := Err[string](errors.New("Foo")).MapI(siconv)
	if a.IsOk() {
		t.Errorf("Err values should not call Mapping")
	}
}

func TestOk(t *testing.T) {
	a := Ok("foobar").Ok()
	if a.IsNone() {
		t.Errorf("Should be some")
	}
}

func TestAndThen(t *testing.T) {
	a := Ok(42).AndThen(func(v int) Result[int] {
		return Ok(v * 2)
	})
	if a.IsErr() {
		t.Errorf("Should be OK")
	}

	if a.Unwrap() != 84 {
		t.Errorf("Expected 84 got %v", a)
	}
}

func TestResult(t *testing.T) {
	ok := Ok(42)
	ok2 := Ok(42)
	ok3 := Ok("foo")
	ok4 := Ok("bar")

	if !ok.Equals(ok2) {
		t.Errorf("Expected equality between %v and %v", ok, ok2)
	}

	if ok3.Equals(ok4) {
		t.Errorf("Did not expected equality between %v and %v", ok3, ok4)
	}

	if ok.IsErr() {
		t.Error("Did not expect Error")
	}

	if !ok.IsOk() {
		t.Error("Expected Ok")
	}

	foo := ok.Unwrap()
	if foo != 42 {
		t.Errorf("Expected 42, got %v", foo)
	}

	e := Err[int](errors.New("some error"))
	if !e.IsErr() {
		t.Error("Expected Err")
	}

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("expected panic on err unwrap")
		}
	}()

	_ = e.Unwrap()
}
