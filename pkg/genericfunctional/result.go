package genericfunctional

import (
	"fmt"
	"reflect"

	"golang.org/x/exp/constraints"
)

type ResultType interface {
	constraints.Ordered
}

// see https://doc.rust-lang.org/std/result/enum.Result.html
type Result[T ResultType] struct {
	inner *T
	err   *error
}

func ResultFrom[T ResultType](v T, err error) Result[T] {
	if err == nil {
		return Ok(v)
	} else {
		return Err[T](err)
	}
}

func (r Result[T]) String() string {
	if r.inner == nil && r.err != nil {
		return fmt.Sprintf("ERR(%v)", *r.err)
	}

	if r.inner != nil {
		return fmt.Sprintf("OK(%v %v)", reflect.TypeOf(*r.inner), *r.inner)
	}

	panic("Neither ERR or OK Result")
}

func (r Result[T]) Equals(other Result[T]) bool {
	if r.inner == nil && other.inner == nil {
		return true
	}

	a, b := *r.inner, *other.inner

	return a == b
}

func Ok[T ResultType](v T) Result[T] {
	return Result[T]{
		inner: &v,
		err:   nil,
	}
}

// The signature is still a bit annoying but we do not have typed enums here
func Err[T ResultType](err error) Result[T] {
	return Result[T]{
		inner: nil,
		err:   &err,
	}
}

func (r Result[T]) IsErr() bool {
	return r.err != nil
}

func (r Result[T]) IsOk() bool {
	return r.inner != nil
}

func (r Result[T]) Unwrap() T {
	if r.inner == nil {
		panic("unwrap on an Err Result")
	}

	return *r.inner
}

func (r Result[T]) UnwrapOr(v T) T {
	if r.inner == nil {
		return v
	}

	return *r.inner
}

func (r Result[T]) UnwrapOrElse(f func() T) T {
	if r.inner == nil {
		return f()
	}

	return *r.inner
}

func (r Result[T]) Ok() Option[T] {
	if r.inner == nil {
		return None[T]()
	}

	return Some(*r.inner)
}

func (r Result[T]) And(v Result[T]) Result[T] {
	if r.inner != nil {
		return v
	}
	return r
}

func (r Result[T]) AndThen(f func(v T) Result[T]) Result[T] {
	if r.inner == nil {
		return r
	}
	return f(*r.inner)
}

func (r Result[T]) IsOkAnd(f func(v *T) bool) bool {
	if r.inner == nil {
		return false
	}
	return f(r.inner)
}

func (r Result[T]) Or(v Result[T]) Result[T] {
	if r.inner == nil {
		return v
	}
	return r
}

func (r Result[T]) OrElse(f func(e error) Result[T]) Result[T] {
	if r.inner == nil {
		return f(*r.err)
	}
	return r
}

func (r Result[T]) Expect(msg string) T {
	if r.inner == nil {
		panic(msg)
	}
	return *r.inner
}

func (r Result[T]) MapI(f func(in T) int) Result[int] {
	if r.inner == nil {
		return Err[int](*r.err)
	}
	return Ok(f(*r.inner))
}

func (r Result[T]) MapU(f func(in T) uint) Result[uint] {
	if r.inner == nil {
		return Err[uint](*r.err)
	}
	return Ok(f(*r.inner))
}

func (r Result[T]) MapF(f func(in T) float64) Result[float64] {
	if r.inner == nil {
		return Err[float64](*r.err)
	}
	return Ok(f(*r.inner))
}

func (r Result[T]) MapS(f func(in T) string) Result[string] {
	if r.inner == nil {
		return Err[string](*r.err)
	}
	return Ok(f(*r.inner))
}

func (r Result[T]) MapErr(f func(e error) error) Result[T] {
	if r.inner == nil {
		return Err[T](f(*r.err))
	}
	return r
}
