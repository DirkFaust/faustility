package genericfunctional

import (
	"fmt"
	"reflect"

	"golang.org/x/exp/constraints"
)

type OptionType interface {
	constraints.Ordered
}

// See https://doc.rust-lang.org/std/option/enum.Option.html
type Option[T OptionType] struct {
	inner *T
}

func (o Option[T]) String() string {
	if o.inner == nil {
		return "None"
	}

	return fmt.Sprintf("Some(%v %v)", reflect.TypeOf(*o.inner), *o.inner)
}

// Equals compares two Option.
// Returns true if the Options are equal.
// Note: standard comparison would compare if the Options are _the same_ which
// normally would lead to a `false` result as the standard comapre would compare the
// inner pointers and not their values.
func (o Option[T]) Equals(other Option[T]) bool {
	if o.inner == nil && other.inner == nil {
		return true
	}

	a, b := *o.inner, *other.inner

	return a == b
}

// The signature is still a bit annoying but we do not have typed enums here
func None[T OptionType]() Option[T] {
	return Option[T]{}
}

func Some[T OptionType](v T) Option[T] {
	return Option[T]{inner: &v}
}

func (o Option[T]) IsSome() bool {
	return o.inner != nil
}

func (o Option[T]) IsNone() bool {
	return !o.IsSome()
}

func (o Option[T]) Expect(msg string) T {
	if o.inner == nil {
		panic(msg)
	}

	return *o.inner
}

func (o Option[T]) Unwrap() T {
	if o.inner == nil {
		panic("tried to unwrap on a None-option")
	}

	return *o.inner
}

func (o Option[T]) UnwrapOr(v T) T {
	if o.inner == nil {
		return v
	}

	return *o.inner
}

func (o Option[T]) UnwrapOrElse(f func() T) T {
	if o.inner != nil {
		return *o.inner
	}

	return f()
}

// this is not satisfying at all... go generics lack aome features yet - we can not pass another generic to the function
func Map[T, U OptionType](from Option[T], f func(in T) U) Option[U] {
	if from.IsNone() {
		return None[U]()
	}

	return Some(f(from.Unwrap()))
}

func (o Option[T]) MapI(f func(in T) int) Option[int] {
	if o.IsNone() {
		return None[int]()
	}

	return Some(f(o.Unwrap()))
}

func (o Option[T]) MapU(f func(in T) uint) Option[uint] {
	if o.IsNone() {
		return None[uint]()
	}

	return Some(f(o.Unwrap()))
}

func (o Option[T]) MapF(f func(in T) float64) Option[float64] {
	if o.IsNone() {
		return None[float64]()
	}

	return Some(f(o.Unwrap()))
}

func (o Option[T]) MapS(f func(in T) string) Option[string] {
	if o.IsNone() {
		return None[string]()
	}

	return Some(f(o.Unwrap()))
}

func (o Option[T]) And(v Option[T]) Option[T] {
	if o.inner == nil {
		return o
	}
	return v
}

func (o Option[T]) AndThen(f func(v T) Option[T]) Option[T] {
	if o.inner == nil {
		return Option[T]{}
	}

	return f(*o.inner)
}

func (o Option[T]) Or(v Option[T]) Option[T] {
	if o.inner != nil {
		return o
	}
	return v
}

func (o Option[T]) OrElse(f func() Option[T]) Option[T] {
	if o.inner != nil {
		return o
	}

	return f()
}

func (o *Option[T]) Take() T {
	if o.inner == nil {
		panic("tried to take a None-option")
	}

	temp := *o.inner
	o.inner = nil
	return temp
}

func (o *Option[T]) Replace(v T) Option[T] {
	temp := o.inner
	o.inner = &v
	return Option[T]{inner: temp}
}

func (o *Option[T]) Insert(v T) *T {
	o.inner = &v
	return o.inner
}

func (o *Option[T]) GetOrInsert(v T) *T {
	if o.inner == nil {
		o.inner = &v
	}
	return o.inner
}

func (o Option[T]) OkOr(err error) Result[T] {
	if o.inner == nil {
		return Err[T](err)
	}

	return Ok(*o.inner)
}

func (o Option[T]) OkOrElse(f func() Result[T]) Result[T] {
	return f()
}
