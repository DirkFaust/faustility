# functional helpers using a generic type

## Use when
You want to have extra type safety by using generics.
If you can or want to live with `any` then use the package `functional` instead.

## WIP!
Contains `Option` and `Result` which I personally miss a lot from rust.
`map`-functionality is generally ugly because go does not support multiple generics or I am too dumb to realize how to do it.
