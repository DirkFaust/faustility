package genericfunctional

import "testing"

func TestOptionalNone(t *testing.T) {
	o := None[int]()
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("the code did not panic")
		}
	}()

	if o.IsSome() {
		t.Errorf("Optional should be None")
	}

	o.Unwrap()
}

func TestSome(t *testing.T) {
	o := Some("foo")

	if o.IsNone() {
		t.Errorf("Optional should be Some")
	}

	if o.Unwrap() != "foo" {
		t.Errorf("unexpected value foo")
	}
}

func TestOr(t *testing.T) {
	o := None[string]()

	if o.IsSome() {
		t.Errorf("Optional should be None")
	}

	if o.UnwrapOr("foo") != "foo" {
		t.Errorf("unexpected value foo")
	}

	a := Some("blah")
	ref := a.GetOrInsert(("baz"))
	if *ref != "blah" {
		t.Errorf("Ref should be blah")
	}
	if a.Unwrap() != "blah" {
		t.Errorf("New should still be blah")
	}

	o = None[string]()
	ref = o.GetOrInsert("baz")
	if *ref != "baz" {
		t.Errorf("Ref should be baz")
	}
	if o.Unwrap() != "baz" {
		t.Errorf("New should still be baza")
	}

	new := o.Insert("bar")
	if *new != "bar" {
		t.Errorf("unexpected value. New should be bar")
	}

	foo := o.Take()
	if foo != "bar" {
		t.Errorf("Take should return 'bar'")
	}
	if o.IsSome() {
		t.Errorf("o should be None after Take()")
	}

	b := Some(42)
	r := b.AndThen(func(v int) Option[int] {
		return Some(v * 2)
	})
	s := Some(84)
	if !r.Equals(s) {
		t.Errorf("AndThen should return %v but got %v", s, r)
	}

	c := None[int]()
	r = c.OrElse(func() Option[int] {
		return Some(42)
	})
	s = Some(42)
	if !r.Equals(s) {
		t.Errorf("OrElse should return %v but got %v", s, r)
	}
}
