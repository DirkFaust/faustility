module gitlab.com/DirkFaust/faustility

go 1.18

require (
	github.com/goiiot/libmqtt v0.9.6
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
	golang.org/x/exp v0.0.0-20220613132600-b0d781184e0d
)

require (
	github.com/klauspost/compress v1.10.3 // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
	nhooyr.io/websocket v1.8.6 // indirect
)
